#!/bin/bash

TMUX=${TMUX:=true}
MINECRAFT_VERSION=${MINECRAFT_VERSION:=1.8.8}
SERVERPROPERTY_LEVEL_NAME=${SERVERPROPERTY_LEVEL_NAME:=world}

SERVER_BASE_DIR=${SERVER_BASE_DIR:=${HOMEDIR}/minecraft}
SERVER_TYPE=${SERVER_TYPE:=spigot}


if [ "${SERVER_TYPE}" == "FORGE" ]; then
    SERVER_BUILD_DIR=${SERVER_BUILD_DIR:=${SERVER_BASE_DIR}/forge_install/${MINECRAFT_VERSION}}
    SERVER_JAR=${SERVER_JAR:=${SERVER_BUILD_DIR}/forge-${MINECRAFT_VERSION}.jar}
    SERVER_OPTS=${SERVER_OPTS:="-server -Xmx2G"}
else
    SERVER_BUILD_DIR=${SERVER_BUILD_DIR:=${SERVER_BASE_DIR}/spigot_build/${MINECRAFT_VERSION}}
    SERVER_JAR=${SERVER_JAR:="${SERVER_BUILD_DIR}/spigot-${MINECRAFT_VERSION}.jar"}
    SERVER_OPTS=${SERVER_OPTS:="-server -Xmx2G"}
fi



SERVER_INDEX=${SERVER_INDEX:=1}
SERVER_NAME="S_${SERVER_INDEX}_${SERVER_TYPE}_V${MINECRAFT_VERSION}_${SERVERPROPERTY_LEVEL_NAME}"
SERVERPROPERTY_SERVER_PORT=${SERVERPROPERTY_SERVER_PORT:=25565}
SERVER_DIR=${SERVER_DIR:=${SERVER_BASE_DIR}/${SERVER_NAME}}
SERVER_PROPERTY_FILE=${SERVER_DIR}/server.properties
SERVER_WORLD_DIR=${WORLD_DIR:=${SERVER_DIR}/${SERVERPROPERTY_LEVEL_NAME}}

FORGE_INSTALLER_URL="https://maven.minecraftforge.net/net/minecraftforge/forge/${MINECRAFT_VERSION}/forge-${MINECRAFT_VERSION}-installer.jar"
SPIGOT_BUILDTOOLS_URL=${SPIGOT_BUILDTOOLS_URL:="https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar"}
TMUX_SESSION=mc

echo "Argument=$1"

echo "MINECRAFT_VERSION=${MINECRAFT_VERSION}"
echo "LEVEL_NAME=${SERVERPROPERTY_LEVEL_NAME}"

echo "SERVER_BASE_DIR=${SERVER_BASE_DIR}"
echo "SERVER_TYPE=${SERVER_TYPE}"
echo "SERVER_BUILD_DIR=${SERVER_BUILD_DIR}"
echo "SERVER_JAR=${SERVER_JAR}"
echo "SERVER_OPTS=${SERVER_OPTS}"


echo "SERVER_NAME=${SERVER_NAME}"
echo "SERVER_INDEX=${SERVER_INDEX}"
echo "SERVER_PORT=${SERVERPROPERTY_SERVER_PORT}"
echo "SERVER_DIR=${SERVER_DIR}"
echo "SERVER_PROPERTY_FILE=${SERVER_PROPERTY_FILE}"
echo "SERVER_WORLD_DIR=${SERVER_WORLD_DIR}"

cd ${HOMEDIR}
ls -al
mkdir -p ${SERVER_BUILD_DIR}


# Pruefen ob die spigot_*.jar bereits erstellt wurde und wenn nicht dann erstellen starten
check_executeable() {
    if [ ! -f ${SERVER_JAR} ]; then
        build
    fi
}

build() {
if [ "${SERVER_TYPE}" == "FORGE" ]; then
    build_forge_jar
else
    build_spigot_jar
fi
}


# Erstellen bzw. Updaten der forge_*.jar
build_forge_jar() {
    echo "install forge executable (${SERVER_JAR})"
    curl -S ${FORGE_INSTALLER_URL} -o ${SERVER_BUILD_DIR}/forge-${MINECRAFT_VERSION}-installer.jar
    pushd ${SERVER_BUILD_DIR} > /dev/null
    java -jar forge-${MINECRAFT_VERSION}-installer.jar --installServer
    popd > /dev/null

    
    if [ $? -ne 0 ]; then
        >&2 echo "Error by building Spigot executable jar file."
        exit 1
    fi
}


# Erstellen bzw. Updaten der spigot_*.jar
build_spigot_jar() {
    echo "Create Spigot executable (${SERVER_JAR})" \
        && curl -S ${SPIGOT_BUILDTOOLS_URL} -o ${SERVER_BUILD_DIR}/BuildTools.jar \
        && pushd ${SERVER_BUILD_DIR} > /dev/null \
        && java -jar BuildTools.jar --rev ${MINECRAFT_VERSION} \
        && popd > /dev/null

    if [ $? -ne 0 ]; then
        >&2 echo "Error by building Spigot executable jar file."
        exit 1
    fi
}


# Pruefen, ob es das SERVER_DIR schon gibt und falls nicht werden alle Default-Files erstellt/kopiert
check_first_run() {
    echo "check first run"
    if [ ! -d ${SERVER_DIR} ]; then
        mkdir -p ${SERVER_DIR}
        
        # Minecraft-EULA akzeptieren.
        if [ "$MINECRAFT_EULA" == "true" ]; then
            echo "Updating $SERVER_DIR/eula.txt"
            echo "eula=$MINECRAFT_EULA" > ${SERVER_DIR}/eula.txt
        else
            >&2 echo "Mojang requires you to accept their EULA. You need to set the MINECRAFT_EULA variable to true."
            exit 1
        fi

        # Defaults mit dem SERVER_DIR synchronisieren      
        rsync -avh --exclude '/world*' ${SERVER_DEFAULT_FILES}/ ${SERVER_DIR}
        
        # wenn es noch keine ops.json gibt wird die ops.txt mit dem Default_OP erzeugt
        # beim ersten Start des Servers wird der darin gespeicherte Benutzer in die ops.json eingetragen
        # Problem ist aber, dass die erzeugte .json erst beim 2. Serverstart zur Anwendung kommt 
        if [ ! -f ${SERVER_DIR}/ops.json ]; then
            if [ -z "${DEFAULT_OP}" ]; then
                >&2 echo "DEFAULT_OP is required. Please set this variable to continue."
                exit 1
            fi
            echo "Adding $DEFAULT_OP to ops list."
            echo "${DEFAULT_OP}" > ${SERVER_DIR}/ops.txt
            echo "${DEFAULT_OP}" > ${SERVER_DIR}/white-list.txt
        fi

    
        # falls noch nicht vorhanden, die server.properties erstellen
        if [ ! -f ${SERVER_PROPERTY_FILE} ]; then
            echo "Creating ${SERVER_PROPERTY_FILE}"
            echo "generator-settings=${SERVERPROPERTY_GENERATOR_SETTINGS}" >> ${SERVER_PROPERTY_FILE}
            echo "op-permission-level=${SERVERPROPERTY_OP_PERMISSION_LEVEL:=4}" >> ${SERVER_PROPERTY_FILE}
            echo "allow-nether=${SERVERPROPERTY_ALLOW_NETHER:=true}" >> ${SERVER_PROPERTY_FILE}
            echo "level-name=${SERVERPROPERTY_LEVEL_NAME}" >> ${SERVER_PROPERTY_FILE}
            echo "enable-query=${SERVERPROPERTY_ENABLE_QUERY:=false}" >> ${SERVER_PROPERTY_FILE}
            echo "allow-flight=${SERVERPROPERTY_ALLOW_FLIGHT:=false}" >> ${SERVER_PROPERTY_FILE}
            echo "announce-player-achievements=${SERVERPROPERTY_ANNOUNCE_PLAYER_ACHIEVEMENTS:=true}" >> ${SERVER_PROPERTY_FILE}
            echo "server-port=${SERVERPROPERTY_SERVER_PORT}" >> ${SERVER_PROPERTY_FILE}
            echo "level-type=${SERVERPROPERTY_LEVEL_TYPE:=DEFAULT}" >> ${SERVER_PROPERTY_FILE}
            echo "enable-rcon=${SERVERPROPERTY_ENABLE_RCON:=false}" >> ${SERVER_PROPERTY_FILE}
            echo "force-gamemode=${SERVERPROPERTY_FORCE_GAMEMODE:=false}" >> ${SERVER_PROPERTY_FILE}
            echo "level-seed=${SERVERPROPERTY_LEVEL_SEED}" >> ${SERVER_PROPERTY_FILE}
            echo "server-ip=${SERVERPROPERTY_SERVER_IP}" >> ${SERVER_PROPERTY_FILE}
            echo "max-build-height=${SERVERPROPERTY_MAX_BUILD_HEIGHT:=256}" >> ${SERVER_PROPERTY_FILE}
            echo "spawn-npcs=${SERVERPROPERTY_SPAWN_NPCS:=true}" >> ${SERVER_PROPERTY_FILE}
            echo "white-list=${SERVERPROPERTY_WHITE_LIST:=true}" >> ${SERVER_PROPERTY_FILE}
            echo "spawn-animals=${SERVERPROPERTY_SPAWN_ANIMALS:=true}" >> ${SERVER_PROPERTY_FILE}
            echo "snooper-enabled=${SERVERPROPERTY_SNOOPER_ENABLED:=true}" >> ${SERVER_PROPERTY_FILE}
            echo "online-mode=${SERVERPROPERTY_ONLINE_MODE:=true}" >> ${SERVER_PROPERTY_FILE}
            echo "resource-pack=${SERVERPROPERTY_RESOURCE_PACK}" >> ${SERVER_PROPERTY_FILE}
            echo "pvp=${SERVERPROPERTY_PVP:=true}" >> ${SERVER_PROPERTY_FILE}
            echo "difficulty=${SERVERPROPERTY_DIFFICULTY:=2}" >> ${SERVER_PROPERTY_FILE}
            echo "enable-command-block=${SERVERPROPERTY_ENABLE_COMMAND_BLOCK:=true}" >> ${SERVER_PROPERTY_FILE}
            echo "player-idle-timeout=${SERVERPROPERTY_PLAYER_IDLE_TIMEOUT:=0}" >> ${SERVER_PROPERTY_FILE}
            echo "gamemode=${SERVERPROPERTY_GAMEMODE:=0}" >> ${SERVER_PROPERTY_FILE}
            echo "max-players=${SERVERPROPERTY_MAX_PLAYERS:=20}" >> ${SERVER_PROPERTY_FILE}
            echo "spawn-monsters=${SERVERPROPERTY_SPAWN_MONSTERS:=true}" >> ${SERVER_PROPERTY_FILE}
            echo "view-distance=${SERVERPROPERTY_VIEW_DISTANCE:=10}" >> ${SERVER_PROPERTY_FILE}
            echo "generate-structures=${SERVERPROPERTY_GENERATE_STRUCTURES:=true}" >> ${SERVER_PROPERTY_FILE}
            echo "motd=${SERVERPROPERTY_MOTD:=A Minecraft Server}" >> ${SERVER_PROPERTY_FILE}
        fi

        # Kopieren des world template falls vorhanden.
        if find ${SERVER_DEFAULT_FILES}/world -mindepth 1 -print -quit | grep -q .; then
            echo "Copy World template to ${SERVER_WORLD_DIR}"
            cp -rf ${SERVER_DEFAULT_FILES}/world* ${SERVER_WORLD_DIR}
        fi
    fi
}

run_spigot() {
#    START_COMMAND="java ${SERVER_OPTS} -jar ${SERVER_JAR} --nogui --world-dir ${SERVER_WORLD_DIR}"
    START_COMMAND="java ${SERVER_OPTS} -jar ${SERVER_JAR} --nogui"
    PARAMS=$@

    echo "TERM is set to ${TERM}"
    cd ${SERVER_DIR}
    pushd ${SERVER_DIR} > /dev/null
    echo "Running Minecraft"
    echo "SERVER_OPTS=${SERVER_OPTS}"
    echo "Extra parameters: ${PARAMS[@]}"

    if [ "${TERM}" == "dumb" ]; then
        >&2 echo "WARNING! Dumb term detected. Switching to noconsole mode."
        >&2 echo "Safe shutdown must be done via /stop chat command."
        START_COMMAND="${START_COMMAND} --noconsole ${PARAMS[@]}"
        echo ${START_COMMAND}
        exec ${START_COMMAND}
    else
        if [ "${TMUX}" == "true" ]; then
            START_COMMAND="${START_COMMAND} ${PARAMS[@]}"
            echo ${START_COMMAND}
            tmux -2 -f ${HOMEDIR}/tmux.conf new -s ${TMUX_SESSION} "${START_COMMAND}"
        else
            >&2 echo "WARNING! Environment variable TMUX is not set 'true'. Switching to noconsole mode."
            >&2 echo "Safe shutdown must be done via /stop chat command."
            START_COMMAND="${START_COMMAND} --noconsole ${PARAMS[@]}"
            echo ${START_COMMAND}
            exec ${START_COMMAND}
        fi
    fi
}

console_command() {
    COMMAND=$@
    if [ "${TERM}" == "dumb" ]; then
        >&2 echo "Console command not supported on a dumb term."
        exit 1
    else
        if [ "${TMUX}" == "true" ]; then
            echo "Executing console command: ${COMMAND[@]}"
            tmux send -t ${TMUX_SESSION} "${COMMAND[@]}" ENTER
        else
            >&2 echo "Console command not supported. TMUX is not set 'true'."
            exit 1
        fi
    fi
}

safe_shutdown() {
    echo "Performing safe shutdown..."
    console_command stop
}

case "$1" in
    run)
        shift 1
        check_executeable
        check_first_run
        trap safe_shutdown EXIT
        run_spigot $@
        ;;
    build)
        shift 1
        build
        ;;
    *)
        exec "$@"
esac
